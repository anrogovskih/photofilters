package org.anro.fotofilters;

/**
 * Created by Анатолий on 02.05.2016.
 */
public enum FiltersTypes {
    Sepia,
    Invert,
    Pixel,
    Sketch,
    Kuwahara,
    Vignette
}

