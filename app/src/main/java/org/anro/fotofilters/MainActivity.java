package org.anro.fotofilters;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.Transformation;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import jp.wasabeef.glide.transformations.gpu.InvertFilterTransformation;
import jp.wasabeef.glide.transformations.gpu.KuwaharaFilterTransformation;
import jp.wasabeef.glide.transformations.gpu.PixelationFilterTransformation;
import jp.wasabeef.glide.transformations.gpu.SepiaFilterTransformation;
import jp.wasabeef.glide.transformations.gpu.SketchFilterTransformation;
import jp.wasabeef.glide.transformations.gpu.VignetteFilterTransformation;

public class MainActivity extends AppCompatActivity {

    private static final int SELECT_PICTURE = 0;
    private LinearLayout thumbnailsContainer;
    private ArrayList<Transformation> filters;
    private static final int TAKE_PHOTO = 1;
    private File photoFile = null;

    private ImageView mImageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        thumbnailsContainer = (LinearLayout) findViewById(R.id.filters);
    }

    //load
    public void onButton1Click (View view)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Select image source")
                .setCancelable(false)
                .setPositiveButton("Gallery",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                loadPhoto();
                            }
                        })
                .setNegativeButton("Camera",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                takePhoto();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    //save
    public void onButton2Click(View view)
    {
        mImageView = (ImageView) findViewById(R.id.imageView);
        mImageView.buildDrawingCache();
        Bitmap bitmap = mImageView.getDrawingCache();
        MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, "test" , "test");
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            //gallery
            if (requestCode == SELECT_PICTURE) {
                Uri selectedImageUri = data.getData();
                setImage(selectedImageUri);
                fillFiltersPanel(selectedImageUri);
            }
            //camera
            if (requestCode == TAKE_PHOTO) {
                setImage(Uri.fromFile(photoFile));
                fillFiltersPanel(Uri.fromFile(photoFile));
            }
        }
    }

    //load image from camera
    private void takePhoto() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                Toast.makeText(this, "An error occurred on file creation", Toast.LENGTH_SHORT).show();
            }
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, TAKE_PHOTO);
            }
        }
    }

    //load image from Gallery
    private void loadPhoto() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,
                "Select Picture"), SELECT_PICTURE);
    }

    //here we create temp file for camera's image to avoid saving it in gallery
    public File createImageFile()throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        return image;
    }

    private void setImage(Uri selectedImageUri) {
        mImageView = (ImageView) findViewById(R.id.imageView);
        mImageView.setImageURI(selectedImageUri);
    }

    //fills filters layout with filtered previews
    public void fillFiltersPanel(final Uri selectedImageUri) {
        thumbnailsContainer.removeAllViews();
        for (final FiltersTypes filter : FiltersTypes.values()) {
            final ImageView imageView = new ImageView(this);
            final float scale = getResources().getDisplayMetrics().density;
            int dpWidthInPx  = (int) (64 * scale);
            int dpHeightInPx = (int) (64 * scale);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(dpWidthInPx, dpHeightInPx);
            imageView.setLayoutParams(layoutParams);


            switch (filter) {
                case Invert:
                    Glide.with(this)
                            .load(selectedImageUri)
                            .bitmapTransform(new InvertFilterTransformation(this))
                            .into(imageView);
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Glide.with(v.getContext())
                                    .load(selectedImageUri)
                                    .bitmapTransform(new InvertFilterTransformation(v.getContext()))
                                    .into(mImageView);
                        }
                    });
                    break;
                case Kuwahara:
                    Glide.with(this)
                            .load(selectedImageUri)
                            .bitmapTransform(new KuwaharaFilterTransformation(this))
                            .into(imageView);
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Glide.with(v.getContext())
                                    .load(selectedImageUri)
                                    .bitmapTransform(new KuwaharaFilterTransformation(v.getContext()))
                                    .into(mImageView);
                        }
                    });
                    break;
                case Pixel:
                    Glide.with(this)
                            .load(selectedImageUri)
                            .bitmapTransform(new PixelationFilterTransformation(this))
                            .into(imageView);
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Glide.with(v.getContext())
                                    .load(selectedImageUri)
                                    .bitmapTransform(new PixelationFilterTransformation(v.getContext()))
                                    .into(mImageView);
                        }
                    });
                    break;
                case Sepia:
                    Glide.with(this)
                            .load(selectedImageUri)
                            .bitmapTransform(new SepiaFilterTransformation(this))
                            .into(imageView);
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Glide.with(v.getContext())
                                    .load(selectedImageUri)
                                    .bitmapTransform(new SepiaFilterTransformation(v.getContext()))
                                    .into(mImageView);
                        }
                    });
                    break;
                case Sketch:
                    Glide.with(this)
                            .load(selectedImageUri)
                            .bitmapTransform(new SketchFilterTransformation(this))
                            .into(imageView);
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Glide.with(v.getContext())
                                    .load(selectedImageUri)
                                    .bitmapTransform(new SketchFilterTransformation(v.getContext()))
                                    .into(mImageView);
                        }
                    });
                    break;
                case Vignette:
                    Glide.with(this)
                            .load(selectedImageUri)
                            .bitmapTransform(new VignetteFilterTransformation(this))
                            .into(imageView);
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Glide.with(v.getContext())
                                    .load(selectedImageUri)
                                    .bitmapTransform(new VignetteFilterTransformation(v.getContext()))
                                    .into(mImageView);

                        }
                    });
                    break;
            }
            thumbnailsContainer.addView(imageView);
        }
    }

}
